package main

import (
	"log"
	"net"

	"google.golang.org/grpc"

	pb "spec"
)

type service struct {
}

func (s *service) Echo(stream pb.Echo_EchoServer) error {
	log.Println("Start streaming to...")
	for {
		msg, err := stream.Recv()
		if err != nil {
			log.Println("End streaming.")
			return err
		}
		log.Printf("Got '%v', sending pong\n", msg.Msg)
		msg.Msg = "pong"
		stream.Send(msg)
	}
}

func main() {
	grpcServer := grpc.NewServer()
	pb.RegisterEchoServer(grpcServer, &service{})

	l, err := net.Listen("tcp", ":5000")
	if err != nil {
		log.Fatalf("Failed to bind to TCP socket: %v", err)
	}

	log.Println("Listening on tcp://localhost:5000")
	grpcServer.Serve(l)
}
