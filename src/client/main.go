package main

import (
	"log"
	"time"

	"golang.org/x/net/context"
	"google.golang.org/grpc"

	pb "spec"
)

func main() {
	conn, err := grpc.Dial("localhost:5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to connect: %s", err)
	}
	defer conn.Close()

	client := pb.NewEchoClient(conn)
	stream, err := client.Echo(context.Background())
	waitc := make(chan struct{})
	msg := pb.Message{Msg: "ping"}

	send := func() {
		time.Sleep(time.Second)
		log.Printf("Sending msg '%v'\n", msg.Msg)
		stream.Send(&msg)
	}

	go func() {
		for {
			resp, err := stream.Recv()
			if err != nil {
				log.Println("End streaming.")
				return
			}
			log.Printf("Got back from server '%v'", resp.Msg)
			send()
		}
	}()
	go send()

	<-waitc
	stream.CloseSend()
}
